import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";

function Example() {
  const [inputs, setInputs] = useState({
    firstname: "",
    lastname: "",
    email: "",
  });
  const [tableData, setTableData] = useState([]);
  const [editClick, setEditClick] = useState(false);
  const [editIndex, setEditIndex] = useState("");
  const validateForm = () => {
    if (inputs.firstname && inputs.lastname && inputs.email) {
      return true;
    } else {
      return false;
    }
  };

  const handleChange = (e) => {
    setInputs({
      ...inputs,
      [e.target.name]: e.target.value,
    });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (!validateForm()) return;
    if (editClick) {
      const tempTableData = tableData;
      Object.assign(tempTableData[editIndex], inputs);
      setTableData([...tempTableData]);
      handleClose();
      setInputs({
        firstname: "",
        lastname: "",
        email: "",
      });
    } else {
      setTableData([...tableData, inputs]);
      handleClose();
      setInputs({
        firstname: "",
        lastname: "",
        email: "",
      });
    }
    // console.log("inputs", inputs);
  };
  const handleEdit = (index) => {
    handleShow();
    const tempData = tableData[index];
    setInputs({
      firstname: tempData.firstname,
      lastname: tempData.lastname,
      email: tempData.email,
    });
    setEditClick(true);
    setEditIndex(index);
  };
  //console.log("tableDta", tableData);
  const handleDelete = (index) => {
    const filterData = tableData.filter((item, i) => i !== index);
    setTableData(filterData);
  };

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <div>
      <h1 className="text-center heading p-3">Crud Operations</h1>
      <Button variant="primary" onClick={handleShow} className="m-4 p-2">
        Add list
      </Button>

      <Modal show={show} onHide={handleClose} onSubmit={handleSubmit}>
        <Modal.Header closeButton>
          <Modal.Title>Crud Operations</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="min-s-screen bg-[#152733] p-2">
            <div className="form-holder">
              <div className="form-content">
                <div className="form-item">
                  <p>Fill in the data below</p>
                  <form onSubmit={handleSubmit}>
                    <div className="flex flex-col mb-3">
                      <input
                        className="form-control"
                        type="text"
                        name="firstname"
                        value={inputs.firstname}
                        onChange={handleChange}
                        placeholder="Enter Firstname"
                      />
                    </div>
                    <div className="flex flex-col mb-3">
                      <input
                        className="form-control"
                        type="text"
                        name="lastname"
                        value={inputs.lastname}
                        onChange={handleChange}
                        placeholder="Enter Lastname"
                      />
                    </div>
                    <div className="flex flex-col mb-3">
                      <input
                        name="email"
                        value={inputs.email}
                        onChange={handleChange}
                        className="form-control"
                        type="text"
                        placeholder="Enter E-mail"
                      />
                    </div>
                    <div className="text-center">
                      <button
                        type="submit"
                        className="btn btn-primary mt-3"
                        id="submit"
                        onClick={handleSubmit}
                      >
                        {editClick ? "Update" : "Add"}
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      <div className="m-4 user-table">
        <table className="w-full text-center table table-bordered">
          <thead>
            <tr>
              <th>#S.NO:</th>
              <th>Firstname</th>
              <th>Lastname</th>
              <th>Email</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody className="text-ash">
            {tableData.map((item, i) => (
              <tr>
                <th scope="row">{i + 1}</th>
                <td>{item.firstname}</td>
                <td>{item.lastname}</td>
                <td>{item.email}</td>
                <td>
                  <button
                    onClick={() => handleEdit(i)}
                    className="btn btn-dark m-1"
                  >
                    Edit
                  </button>

                  <button
                    onClick={() => handleDelete(i)}
                    className="btn btn-danger"
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default Example;
